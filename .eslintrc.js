module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ["plugin:vue/vue3-essential", "eslint:recommended"],

  parser: "vue-eslint-parser",
  parserOptions: {
    vueFeature: {
      filter: false,
      interpolationAsNonHTML: true,
      styleCSSVariableInjection: true,
      endOfLine: "auto",
    },
    ecmaVersion: 2020,
  },
  rules: {
    "no-unused-vars": "off",
  },
};
