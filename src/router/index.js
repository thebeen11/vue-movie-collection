import { createRouter, createWebHashHistory } from "vue-router";
import { AddMovie, EditMovie, Home, Layout } from "../views";

const routes = [
  {
    path: "/",
    name: "home",
    component: Layout,
    redirect: "/",
    children: [
      { path: "/", name: "Movies Collection", component: Home },
      { path: "/addmovie", name: "Add Movie", component: AddMovie },
      { path: "/viewdetail", name: "View Detail", component: EditMovie },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
