export { default as Layout } from "./Layout/Layout.vue";
export { default as Home } from "./Home/Home.vue";
export { default as AddMovie } from "./AddMovie/AddMovie.vue";
export { default as EditMovie } from "./AddMovie/EditMovie.vue";
