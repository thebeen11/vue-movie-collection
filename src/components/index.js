export { default as Navbar } from "./Navbar/Navbar.vue";
export { default as CardMovie } from "./Card/CardMovie.vue";
export { default as EmptyCard } from "./Card/EmptyCard.vue";
