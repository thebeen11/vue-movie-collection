import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { Quasar, Notify, Dialog } from "quasar";
import quasarUserOptions from "./quasar-user-options";
import { GLOBAL_MIXIN } from "./mixin";

const app = createApp(App);

app.config.globalProperties.$filters = {
  subString(value) {
    if (value.length > 70) {
      value = value.substring(70, 0) + "...";
      return value;
    } else {
      return value;
    }
  },
};
app.mixin(GLOBAL_MIXIN);
app
  .use(
    Quasar,
    {
      plugins: {
        Notify,
        Dialog,
      },
    },
    quasarUserOptions
  )
  .use(store)
  .use(router);

app.mount("#app");
