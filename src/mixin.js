export const GLOBAL_MIXIN = {
  computed: {
    $moviesData() {
      return (
        JSON.parse(localStorage.getItem("moviesCollection")) || { data: [] }
      );
    },
    $genreOptions() {
      let genre = [
        { label: "Drama", value: "drama", selected: false },
        { label: "Action", value: "action", selected: false },
        { label: "Animation", value: "animation", selected: false },
        { label: "Sci-Fi", value: "scifi", selected: false },
        { label: "Horor", value: "horor", selected: false },
      ];
      return genre;
    },
  },
  methods: {},
};
