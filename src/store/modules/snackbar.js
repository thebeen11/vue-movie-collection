import { Notify } from "quasar";

const state = {
  data: [],
};

const mutations = {
  SET_DATA: (state, data) => {
    state.data = data;
  },
};

const actions = {
  setSnackbar({ commit }, data) {
    Notify.create({
      position: "top",
      type: data.type,
      message: data.message,
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
