import { Dialog } from "quasar";

const state = {
  data: [],
};

const mutations = {
  SET_DATA: (state, data) => {
    state.data = data;
  },
};

const actions = {
  setDialog({ commit }, data) {
    return new Promise((resolve, reject) => {
      Dialog.create({
        title: "Confirm",
        message: data.message,
        cancel: true,
        persistent: true,
      })
        .onOk(() => {
          resolve(true);
        })
        .onCancel(() => {
          resolve(false);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
