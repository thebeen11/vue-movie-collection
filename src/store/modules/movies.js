const state = {};

const mutations = {};

const actions = {
  postMovie({ commit }, payload) {
    let tmp = {
      data: [],
    };
    tmp = JSON.parse(localStorage.getItem("moviesCollection")) || { data: [] };
    tmp.data.push(payload);

    localStorage.setItem("moviesCollection", JSON.stringify(tmp));
  },
  getMovieById({ commit }, params) {
    let tmp = {
      data: [],
    };
    tmp = JSON.parse(localStorage.getItem("moviesCollection")) || { data: [] };
    let result = tmp.data.find((item) => item.id == params);
    return result;
  },
  updateMovie({ commit }, payload) {
    let tmp = {
      data: [],
    };
    let result = {
      data: [],
    };
    tmp = JSON.parse(localStorage.getItem("moviesCollection")) || { data: [] };

    result.data = tmp.data.filter((item) => item.id !== payload.id);
    result.data.push(payload);
    localStorage.setItem("moviesCollection", JSON.stringify(result));
  },
  deleteMovie({ commit }, id) {
    let tmp = {
      data: [],
    };
    let result = {
      data: [],
    };
    tmp = JSON.parse(localStorage.getItem("moviesCollection")) || { data: [] };

    result.data = tmp.data.filter((item) => item.id != id);

    localStorage.setItem("moviesCollection", JSON.stringify(result));
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
